<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 1</title>
</head>
<body>

	<h2>Full Address</h2>

	<p><?php echo getFullAddress(513, 'N.Roxas Street', 'San Roque', 'Marikina City', 'Metro Manila', 'Philippines');?></p>

	<p><?php echo getFullAddress(235, 'JP Rizal Street', 'Kalumpang', 'Antipolo City', 'Rizal', 'Philippines');?></p>

	<h2>Letter Based Grading</h2>

	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>
	

</body>
</html>